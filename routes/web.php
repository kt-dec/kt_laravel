<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Define a route wherein we can view create psot and it will be returned to the user
Route::get('/posts/create', [PostController::class, 'create']);

// Define 

Route::post('/posts', [PostController::class, 'store']);

// Define route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

// Define a route that will return view containing only the authenticated user's posts
Route::get('/myPosts', [PostController::class, 'myPost']);

// Define a route wherein a view showing a specific post with matching URL parameter ID and will be returned to the user
Route::get('posts/{id}', [PostController::class, 'show']);

// define a route for viewing the edit
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching URL parameter ID viw PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

// Define a route for deleting a post
Route::delete('/posts/{id}', [PostController::class, 'destroy']);

// Define 
Route::put('posts/{id}/like', [PostController::class, 'like']);